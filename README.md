# Event Manager

## Technical test

## Description

This project is made of 3 main components :
- A MongoDB API that interacts with the database through 3 endpoints :
	```
	* `/add_event` : creating a new event
	* `/list_events` : listing existing events
	* `/remove_events` : deleting all events
	```

- A MongoDB CLI client that has 3 commands as well and calls the API server :
	```
	* add-event : creating a new event
	* list-events : listing existing events
	* remove-events : deleting all events
	```
- Finally an event server that acts as a functionnal testing stub, the usecase I imagined would be an event manager server that controls instances on a cloud, let's say AWS EC2 instances for example. It has 3 endpoints as well :
	```
	* /stream?param=start
	* /stream?param=run
	* /stream?param=stop
	```
The stub's role is to add a functionnal aspect while interacting with the database.
As for the MongoDB itself, I chose to use a free cluster on the MongoDB cloud to gain time.

## Installation

This project was tested in a Python 3.7 environment, I expect it to be compatible with Python 3.7+

To run project first execute :
```
pip install -r requirements.txt
```

A testing script was made in bash (tested in a MacOS environment but should be compatible with all Unix envs). The script will bring up the project servers and print instructions for the CLI client.

```
sudo bash test-project.sh
```
This script will launch both the MongoDB API (port:8000) and the Stub event server (port:5000) on the localhost.

## Usage

Example commands to use the CLI client :

- Create an event :

```
python3 mongo_cli.py add-event "some tag"
```

- List all events :

```
python3 mongo_cli.py list-events
```

- Delete all events :

```
python3 mongo_cli.py remove-events --force
```

=> After executing test-project.sh =>

To check out the API documentation, please visit :
```
http://localhost:8000/docs
```

It is also possible to interact with the stub/mock event server by visiting the following endpoints :

```
http://localhost:5000/stream?param=start
http://localhost:5000/stream?param=run
http://localhost:5000/stream?param=stop
```
Each of these actions on event server will add a new event in the MongoDB.

**Warning** :
When finishing the tests, it is best to run :
```
sudo kill -9 `pgrep -f gunicorn`
```
This should stop all the apps/servers running in the background.

## Project status
This test was a first for me in many regards, as I've never used MongoDB, FastAPI and the associated technologies before.

It has also been quite a busy week for me, so I had very little time to work on the test.
What I would've done differently given more time :

- Paid more attention to good practices when it comes to async dev but also the repartition of class objects versus modules, among others.

- Pakcaged the project in a docker container or such to make it more easily installable and testable.

- Been more serious with my commits on the project.

- Implemented a real solution rather using a stub.

- Explored solutions based on Celery and other technologies used at BS.

## Acknowledgments / Credits

Loosely based on :

-[mongodb-with-fastapi](https://github.com/mongodb-developer/mongodb-with-fastapi)

-[typer-code-snippets](https://github.com/tiangolo/typer)

-[server-send-events-with-fastapi](https://sairamkrish.medium.com/handling-server-send-events-with-python-fastapi-e578f3929af1)
