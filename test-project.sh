echo "starting MongoDB API server..."
gunicorn --worker-class uvicorn.workers.UvicornWorker --bind '127.0.0.1:8000' --daemon mongo_api:app

echo "starting stub event server..."

gunicorn --worker-class uvicorn.workers.UvicornWorker --bind '127.0.0.1:5000' --daemon event_server:router 

echo "printing MongoDB CLI client manual :"
python3 mongo_cli.py --help