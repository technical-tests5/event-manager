from sse_starlette.sse import EventSourceResponse
from fastapi import APIRouter, Request
import time
import logging
import asyncio
import requests

"""
Testing stub
Mock HTTP server to generate fake events
"""

router = APIRouter()
status_stream_delay = 10  # second
tags = []
data = {
        "start" : "",
        "stop" : "",
        "tags" : tags,
        "msg" : ""
        }
logger = logging.getLogger('Event manager')

def compute_status(param):
    """
    Change event status and data according to the param indicated in request

    Args :
        param : request stage indication
    """
    global data
    global tags
    status = None
    data.pop("msg")
    if param == "start" :
        data["start"] = str(int(time.time()))
        status = "creating instance"
        if not tags :
            tags.append(status)
        else :
            tags = [status]
        response = requests.post(url='http://127.0.0.1:8000/add_event', json=data)
        data["msg"] = "a new instance was created"
    elif param == "run" :
        status = "running"
        if ("creating instance" in tags) and (not status in tags) :
            tags.append(status)
            response = requests.post(url='http://127.0.0.1:8000/add_event', json=data)
            data["msg"] = "an instance is up and running"
        else :
        	data["msg"] = "no existing instance to run"
    elif param == "stop" :
        status = "stopped"
        if ("creating instance" in tags) and (not status in tags) :
            tags.append(status)
            data["stop"] = str(int(time.time()))
            response = requests.post(url='http://127.0.0.1:8000/add_event', json=data)
            data["msg"] = "an instance was stopped"
        else :
        	data["msg"] = "no running instance to stop"
    
    return status

        

async def event_generator(request, param):
    """
    Generates new event and updates status and data

    Args :
        request : request made to event server
        param : request stage indication
    """
    global data
    global tags
    previous_status = None
    while True:
        if await request.is_disconnected():
            logger.debug('Request disconnected')
            break

        current_status = compute_status(param)
        if previous_status != current_status :
            yield {
                "event": "update",
                "data": data
            }
            logger.debug('Current status :%s', current_status)
            previous_status = current_status
            if previous_status=='stopped':
                logger.debug('Request completed. Disconnecting now')
                tags = []
                data = {
                        "start" : "",
                        "stop" : "",
                        "tags" : tags,
                        "msg" : "Request completed. Disconnecting now"
                        }
                yield {
                    "event": "end",
                    "data": data                
                }
            break

        else:
            logger.debug('No change in status...')

        await asyncio.sleep(status_stream_delay)

@router.get('/stream')
async def runEventServer(
        param: str,
        request: Request
):
    """
    Mock HTTP server request to test event generation and communicate with mongo db

    Args :
        param : request param to indicate event stage
        request : request to run on server
    """

    return EventSourceResponse(event_generator(request, param))