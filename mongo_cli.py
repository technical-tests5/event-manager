import typer
import time
import requests
import json

app = typer.Typer(help="CLI client to manage MongoDB")

@app.command()
def add_event(tag: str):
    """
    Create a new event with tag argument
    """

    new_event = {
                "start": str(int(time.time())),
                "stop": "",
                "tags": [tag, "added through CLI"],
                }
    response = requests.post(url='http://127.0.0.1:8000/add_event', json=new_event)
    if str(response.status_code).startswith("20"):
        print(f"Creating event with tag {tag} :")
        pretty_json = json.loads(response.text)
        print (json.dumps(pretty_json, indent=2))
    else :
        print(f"Could not create event due to HTTP error code {response.status_code}")


@app.command()
def list_events():
    """
    List all events in db
    """
    response = requests.get(url='http://127.0.0.1:8000/list_events')
    if str(response.status_code).startswith("20"):
        print("Listing all events")
        pretty_json = json.loads(response.text)
        print (json.dumps(pretty_json, indent=2))
    else :
        print(f"Could not list events due to HTTP error code {response.status_code}")



@app.command()
def remove_events(
    force: bool = typer.Option(
        ...,
        prompt="Are you sure you want to delete ALL events?",
        help="Force deletion without confirmation.",
    )):
    """
    Delete ALL users in the database.

    If --force is not used, will ask for confirmation.
    """
    if force:
        response = requests.delete(url='http://127.0.0.1:8000/remove_events')
        if str(response.status_code).startswith("20"):
            print("Deleting all events successfully")
        else :
            print(f"Could not remove events due to HTTP error code {response.status_code}")
    else:
        print("Operation cancelled")


if __name__ == "__main__":
    app()
