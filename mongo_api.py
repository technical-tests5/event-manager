import os
from fastapi import FastAPI, Body, HTTPException, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, Field, EmailStr
from bson import ObjectId
from typing import Optional, List
import motor.motor_asyncio


app = FastAPI()
USERNAME = ""
PASSWORD = ""
MONGODB_URL = f"mongodb+srv://{USERNAME}:{PASSWORD}@cluster0.25ifcob.mongodb.net/?retryWrites=true&w=majority"
client = motor.motor_asyncio.AsyncIOMotorClient(MONGODB_URL)
db = client.data


class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError("Invalid objectid")
        return ObjectId(v)

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type="string")


class EventModel(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    start: str = Field(...)
    stop: str = Field(...)
    tags: list = Field(...)

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "start": "1658436654",
                "stop": "1658436665",
                "tags": ["data1", "data2", "data3"],
            }
        }


# class UpdateEventModel(BaseModel):
#     start: Optional[str]
#     stop: Optional[str]
#     tags: Optional[list]

#     class Config:
#         arbitrary_types_allowed = True
#         json_encoders = {ObjectId: str}
#         schema_extra = {
#             "example": {
#                 "start": "1658436654",
#                 "stop": "1658436665",
#                 "tags": ["data1", "data2", "data3"],
#             }
#         }


@app.post("/add_event", response_description="Add new event", response_model=EventModel)
async def add_event(event: EventModel = Body(...)):
    event = jsonable_encoder(event)
    new_event = await db["events"].insert_one(event)
    created_event = await db["events"].find_one({"_id": new_event.inserted_id})
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=created_event)


@app.get(
    "/list_events", response_description="List all events", response_model=List[EventModel]
)
async def list_events():
    events = await db["events"].find().to_list(1000)
    return events


@app.delete("/remove_events", response_description="Delete all events")
async def remove_events():
    delete_result = await db["events"].delete_many({})

    if delete_result.deleted_count >= 1:
        return JSONResponse(status_code=status.HTTP_204_NO_CONTENT)

    raise HTTPException(status_code=404, detail="No events found")

